новый солюшен

```
dotnet new sln
```

создаем библиотеку и добавляем ее к солюшену

```
dotnet new classlib -o FBLib
dotnet sln add .\FBLib\FBLib.csproj
```

создаем проект с тестами и добавляем ее к солюшену

```
dotnet new xunit -o FBTest
dotnet sln add .\FBTest\FBTest.csproj
```

добавляем ссылку на основной проект в тестовый проект

```
dotnet add FBTest/FBTest.csproj reference FBLib/FBLib.csproj
```
