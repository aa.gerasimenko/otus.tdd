﻿
namespace FBLib;

public class FizzBuzz
{
    public static string Write(int number)
    {
        return number switch
        {
            _ when number.IsDivisible(15) => "FizzBuzz",
            _ when number.IsDivisible(3) => "Fizz",
            _ when number.IsDivisible(5) => "Buzz",
            _ => number.ToString()
        };

        // if (number.IsDivisible(15))
        //     return "FizzBuzz";
        // if (number.IsDivisible(3))
        //     return "Fizz";
        // if (number.IsDivisible(5)) // можем выделить отдельный метод, чтобы уменьшить вероятность ошибки
        //     return "Buzz";
        // return number.ToString();
    }
}

public static class IntExt
{
    public static bool IsDivisible(this int value, int number) =>
        value % number == 0;
}
