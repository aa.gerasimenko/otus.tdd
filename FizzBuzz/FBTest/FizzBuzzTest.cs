using FBLib;

namespace FBTest;

public class FizzBuzzTests
{
    [Theory]
    [InlineData(1)]
    [InlineData(4)]
    [InlineData(77)]
    public void GivenNumberNotDivisibleBy3or5_ThenReturnNumberAsString(int number)
    {
        string result = FizzBuzz.Write(number);

        Assert.Equal(number.ToString(), result);
    }

    [Theory]
    [InlineData(5)]
    [InlineData(10)]
    [InlineData(55)]
    public void GivenNumberDivisibleBy5_ThenReturnBuzz(int number)
    {
        string buzz = "Buzz";
        string result = FizzBuzz.Write(number);

        Assert.Equal(result, buzz);
    }

    [Theory]
    [InlineData(3)]
    [InlineData(6)]
    [InlineData(9)]
    public void GivenNumberDivisibleBy3_ThenReturnFizz(int number)
    {
        string fizz = "Fizz";
        string result = FizzBuzz.Write(number);

        Assert.Equal(result, fizz);
    }


    [Theory]
    [InlineData(15)]
    [InlineData(30)]
    [InlineData(60)]
    public void GivenNumberDivisibleBy3and5_ThenReturnFizzBuzz(int number)
    {
        string fizz = "FizzBuzz";
        string result = FizzBuzz.Write(number);

        Assert.Equal(result, fizz);
    }
}