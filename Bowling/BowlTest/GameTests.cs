using BowlLib;

namespace BowlTest;

public class GameTests
{
    Game g;

    public GameTests()
    {
        g = new();
    }

    [Fact]
    public void GivenRollShootNonePins_ThenScore0()
    {
        RollSame(20, 0);
        Assert.Equal(0, g.Score());
    }

    [Fact]
    public void GivenRollShoot1Pin_ThenScoreTotalPins()
    {
        RollSame(20, 1);
        Assert.Equal(20, g.Score());
    }

    [Fact]
    public void GivenRollShootWith1Spare_ThenScoreShouldUseNextRoll()
    {
        RollSpare(); // spare: 10 +3 
        g.Roll(3);      //        +3
        RollSame(17, 0);
        Assert.Equal(16, g.Score());
    }

    [Fact]
    public void GivenRollShootWith1Strike_ThenScoreShouldUseNext2Rolls()
    {
        g.Roll(10);     // spare: 10 +3 +1
        g.Roll(3);      //        +3
        g.Roll(1);      //        +1
        RollSame(16, 0);
        Assert.Equal(18, g.Score());
    }

    [Fact]
    public void GivenRollShootWith12Strikes_ThenScoreShouldMaximumScore()
    {
        RollSame(12, 10);
        Assert.Equal(300, g.Score());
    }

    private void RollSame(int rollNumber, int pins)
    {
        for (int i = 0; i < rollNumber; i++)
            g.Roll(pins);
    }

    private void RollSpare()
    {
        g.Roll(5);
        g.Roll(5);
    }

}